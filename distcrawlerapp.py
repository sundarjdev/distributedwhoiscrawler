import node
import netifaces
import socket
import struct
import logging
import threading
import subprocess
import time
import radix
from time import strftime, gmtime
import os
import xmlrpclib
from SimpleXMLRPCServer import SimpleXMLRPCServer

import cPickle as pickle
import glob
import sys, hashlib, sha3

from netaddr import *

# class to find all active nodes in the subnet
class FindActiveNodes:
    def __init__(self):
        # empty list for storing active nodes found
        self.active_nodes_list = []

        # find all active nodes and store in list
        self.find_all_active_nodes()

        # find my own IP
        self.find_own_ip()

    # Method to find all active nodes by reading the config file
    def find_all_active_nodes(self):
        # open the config file to read active node list
        lines = open('nodes.config').read().splitlines()
        for i in range(len(lines)):
            ip = '%s' % (IPAddress(lines[i]))
            self.active_nodes_list.append(ip)

    # Method to find my own IP
    def find_own_ip(self):
        myiface = 'eth0'
        addrs = netifaces.ifaddresses(myiface)
        ipinfo = addrs[socket.AF_INET][0]
        address = ipinfo['addr']
        self.myip = '%s' % (address)

# class for leader election
class LeaderElection:
    def __init__(self, active_nodes_list, curr_leader):
        self.iplist = active_nodes_list
        # start with first node as leader
        if curr_leader == 0:
            self.leader = self.iplist[0]
        else:
            self.leader = curr_leader

        # elect the highest IP node as leader
        self.old_leader = curr_leader
        self.elect_leader()

    # method converts IP address to long integer
    def ip2long(self, ip):
        packedIP = socket.inet_aton(ip)
        return struct.unpack("!L", packedIP)[0]

    # ping test
    def ping_test(self):
        hostname = str(self.old_leader)
        response = os.system("ping -c 1 " + hostname)

        #and then check the response...
        if response == 0:
          return True
        else:
          return False 

    # method that does the actual leader election
    def elect_leader(self):
        if not self.old_leader == 0:
            ret = self.ping_test()
            if ret == False:
		self.iplist.remove(self.old_leader)
                for i in self.iplist:
                    # select the highest numbered IP as leader
                    if self.ip2long(i) > self.ip2long(self.leader):
                        self.leader = i

        else:
            for i in self.iplist:
                # select the highest numbered IP as leader
                if self.ip2long(i) > self.ip2long(self.leader):
                    self.leader = i

# class for running distributed crawler
class DistributedCrawler:
    def __init__(self):
        self.iter = 0
        # Hash variables to check for differences in CIDRS files
        self.CIDRFormerHash = ''
	s = threading.Thread(target=self.start_chord)	
	s.start()

    def start_chord(self):
	# CHORD
	myownip = self.find_own_ip()
	print 'IP:'+str(myownip)+', hash: '+str(hash(myownip))+', hashId: '+str(self.hashId(str(hash(myownip))))

	h = hashlib.sha3_256()
        h.update(myownip)

	print h.hexdigest()
	print self.hashId(str(h.hexdigest()))
	self.nodeA = node.Node(self.hashId(str(h.hexdigest())), 'http://' +  str(myownip) + ':8000')
	with open('nodes.config', 'r') as fin:
		chordNode = fin.readline()[:-1]
	self.nodeA.join('http://' + str(chordNode) + ':8000')

    # Method to find my own IP
    def find_own_ip(self):
        myiface = 'eth0'
        addrs = netifaces.ifaddresses(myiface)
        ipinfo = addrs[socket.AF_INET][0]
        address = ipinfo['addr']
        return '%s' % (address)

    def find_nodes(self):
        # find all active nodes
        activeNodes = FindActiveNodes()
        self.node_list = activeNodes.active_nodes_list
        self.myip = activeNodes.myip

    # starts a XML RPC server thread on the current node.
    # Dependency: should be called only after calling find_nodes()
    def start_xmlrpc_server(self):
        XMLRPCServerThread = threading.Thread(target=self.XMLRPCServer)
        XMLRPCServerThread.start()

    def read_file(self, file):
        with open(file, "rb") as handle:
             return xmlrpclib.Binary(handle.read())

    def store_file(self, file, bin):
        with open(file, "wb") as handle:
            handle.write(bin.data)

    def hashId(self, date):
        h = 0
        for i in range(len(date)-1):
            h = (31*h+ord(date[i]))%32
        return h

    def archive_file(self, oldfilename, archivefilename):
        os.rename(oldfilename, archivefilename)
        # check with chord if I need to store this file
        date = oldfilename[6:]
        h1 = self.hashId(str(date)+str(date))
        h2 = self.hashId(str(date)+str(h1))
        h3 = self.hashId(str(date)+str(h2))

        self.log('notice', 'hash('+str(date)+str(date)+'): '+str(h1))
        self.log('notice', 'hash('+str(date)+str(h1)+'): '+str(h2))
        self.log('notice', 'hash('+str(date)+str(h2)+'): '+str(h3))
        if True == keepData(h1, self.myip):
            self.log('notice', 'Worker('+str(self.myip)+'): Chord wants me to keep archive file: '+str(archivefilename))
        elif True == keepData(h2, self.myip):
            self.log('notice', 'Worker('+str(self.myip)+'): Chord wants me to keep archive file: '+str(archivefilename))
        elif True == keepData(h3, self.myip):
            self.log('notice', 'Worker('+str(self.myip)+'): Chord wants me to keep archive file: '+str(archivefilename))
        else: # remove the file
            self.log('notice', 'Worker('+str(self.myip)+'): Chord wants me to delete archive file: '+str(archivefilename))
            os.remove(archivefilename)

    def delete_file(self, file):
        os.remove(file)

    def XMLRPCServer(self):
        server = SimpleXMLRPCServer(('%s' % (self.myip), 7699), allow_none=True)
        server.register_function(self.read_file, "read_file")
        server.register_function(self.store_file, "store_file")
        server.register_function(self.archive_file, "archive_file")
        server.register_function(self.delete_file, "delete_file")
        server.serve_forever()

    def crawler(self):
        self.log('notice', 'DistributedCrawler: Iteration:'+str(self.iter))
        # elect highest IP number as leader/master node
        l = LeaderElection(self.node_list, self.leader)
	self.node_list = l.iplist
        self.leader = l.leader

        # sort active node list
        self.node_list.sort()

        self.log('notice', 'DistributedCrawler: Active Nodes list: %s' % (self.node_list))
        print "Active Nodes list: %s " % (self.node_list)
        self.log('notice', 'DistributedCrawler: my IP: %s' % (self.myip))
        print "my IP: %s " % (self.myip)
        self.log('notice', 'DistributedCrawler: Elected Leader for this iteration: %s' % (self.leader))
        print "elected leader: %s " % (self.leader)

        MasterThread = threading.Thread(target=self.MasterNode)
        WorkerThread = threading.Thread(target=self.WorkerNode)

        if self.myip == self.leader: # Leader/Master node
            # Master node starts both worker thread and master thread
            WorkerThread.start()
            MasterThread.start()
            WorkerThread.join()
            MasterThread.join()
        else: # Worker node
            # Worker node starts only Worker thread
            WorkerThread.start()
            WorkerThread.join()

    # Master node function
    def MasterNode(self):
        import select
        print "Master node at work"
        self.log('notice', 'Master node at work!')

        # split crawling IP range among active nodes
        self.num_nodes = len(self.node_list)
        ip_inc = 2 // self.num_nodes # floor of division
        self.subprocess = []

        for i in range(self.num_nodes):
            ip = '%s.0.0.0' % ((i*ip_inc))
            self.subprocess.append(ip)
            # last node should crawl all remaining IPs all the way upto 255.0.0.0
            if i == (self.num_nodes-1):
                ip = '2.0.0.0'
                self.subprocess.append(ip)

        print "crawling range: %s " % (self.subprocess)
        self.log('notice', 'DistributedCrawler: Crawling range: %s' % (self.subprocess))
        print "number of workers: %d" % (self.num_nodes)
        self.log('notice', 'DistributedCrawler: Number of Workers: %d' % (self.num_nodes))

        # backup list for run-time manipulation of IP ranges due to worker node failures
        subprocessIPs = []
        subprocessIPs = list(self.subprocess)

        self.actualNumOfWorkers = self.num_nodes
        self.numOfWorkersComplete = 0
        sock = []
        inputs = []
        outputs = []

        for i in xrange(self.num_nodes):
            WorkerAddr = ('%s' % (self.node_list[i]), 6669)
            try:
                s = socket.create_connection(WorkerAddr, 10) # with a 10 sec timeout, master node should be startied last
                sock.append(s)
                inputs.append(sock[-1])
                # Send msg=process_num|firstIP|lastIP
                message = str(i)+'|'+str(subprocessIPs[i])+'|'+str(self.subprocess[i+1])
                sock[-1].sendall(message)
            except Exception as e:
                print 'ERROR: Master failed to connect to Worker('+self.node_list[i]+')'+str(e)
                self.log('error','Master failed to connect to Worker('+self.node_list[i]+')'+str(e))

                self.actualNumOfWorkers = self.actualNumOfWorkers-1
                if (i != self.num_nodes-1):
                    print 'WARNING: Master reassigning IP range to next Worker'
                    self.log('warning', 'WARNING: Master reassigning IP range to next Worker')
                    subprocessIPs[i+1] = subprocessIPs[i]
                else :
                    print 'ERROR: Last worker is dead! No more workers to assign the remaining IP range!'
                    self.log('error','ERROR: Last worker is dead! No more workers to assign the remaining IP range!')

        running = 1
        while running:
            readable, writable, error = select.select(inputs, outputs, inputs)
            for s in readable:
                data = s.recv(1024)
                if data:
                    # DO SOMETHING WITH THE DATA RECEIVED
                    dataArray = data.split('|')
                    if dataArray[0] == 'DONE':
                        n = int(dataArray[1])
                        print "Master received DONE from worker: %d" % (n)
                        self.log('notice', 'Master received DONE from worker'+str(n))

                        # Copy the pickle CIDR file and queue file to local file
                        proxy = xmlrpclib.ServerProxy('http://'+str(self.node_list[n])+':7699')
                        with open('_CIDR_'+str(n), "wb") as handle:
                            handle.write(proxy.read_file('CIDR_'+str(n)).data)
                        with open('_QUEUE_'+str(n), "wb") as handle:
                            handle.write(proxy.read_file('QUEUE_'+str(n)).data)

                        # And delete the pickle CIDR and queue files from this worker
                        proxy.delete_file('CIDR_'+str(n))
                        proxy.delete_file('QUEUE_'+str(n))

                        self.numOfWorkersComplete += 1
                        # remove socket from inputs list
                        inputs.remove(s)
                        s.shutdown(socket.SHUT_RDWR)
                        s.close()

                    else: # if data != 'DONE' 
                        print "ERROR: Master received invalid message from worker"
                        self.log('error', 'Master received invalid message from worker')

                else: # if no data
                    print "WARNING: Worker closed connection with me (Master)"
                    self.log('warning','Worker closed connection with me (Master)')

                    self.numOfWorkersComplete += 1
                    # remove socket from inputs list
                    inputs.remove(s)
                    s.shutdown(socket.SHUT_RDWR)
                    s.close()

                # If all Workers are done
                if self.numOfWorkersComplete == self.actualNumOfWorkers:
                    # start merging all pickles
                    self.mergeIndividualPickles()

                    running = 0
                    self.numOfWorkersComplete = 0   # reset the counter
                    print '*******Master PROCESS DONE!*******'
                    self.log('notice','*******Master PROCESS DONE!********')
                    self.iter += 1 # increment iter count to start next iteration. We do this even if some workers fail.
                    print 'Iteration %d completed successfully!' % (self.iter)
                    self.log('notice','Iteration'+str(self.iter)+' completed successfully!')
                    break

    # API that merges all pickles together
    def mergeIndividualPickles(self):
        self.log('notice', 'DistMaster::mergeIndividualPickles('+str(self.iter)+'): Collecting data...')
        tree = radix.Radix()
        temp = radix.Radix()
        queue = []
        cidrFilesMerged = 0
        queueFilesMerged = 0
        
        # Get all tree_states created:
        for indTree in glob.glob('_CIDR_*'):
            temp = pickle.load(open(indTree, 'rb'))
            for node in temp:
                n = tree.add(node.prefix)
                n.data['asn'] = node.data['asn']
                n.data['cc'] = node.data['cc']
                n.data['reg'] = node.data['reg']
                n.data['isp'] = node.data['isp']
            os.remove(indTree)
            cidrFilesMerged += 1
        for indQueue in glob.glob('_QUEUE_*'):
            queue += pickle.load(open(indQueue, 'rb'))
            os.remove(indQueue)
            queueFilesMerged += 1
        self.log('notice', 'DistMaster::mergeIndividualPickles('+str(self.iter)+'): Data collected. Saving...')
        
        '''
        CHECK IF ANYTHING HAS CHANGED AND ARCHIVE THE OLD CIDR
        '''
        # Generate the tree digest:
        h = hashlib.sha3_256()
        h.update(pickle.dumps(tree))
        
        # Check if something has changed:
        if not h.hexdigest() == self.CIDRFormerHash:
            self.log('notice', 'DistMaster::mergeIndividualPickles('+str(self.iter)+'): A difference was spotted in the tree.')
            # Archive the old CIDR if exists
            oldCidrFilename = glob.glob('CIDRS*')    # get the CIDRS filename
            if len(oldCidrFilename) == 1:   # if an older CIDRS file exists
                self.log('notice', 'DistMaster::mergeIndividualPickles('+str(self.iter)+'): Archiving old CIDRS file.')
                filename = oldCidrFilename[0]
                #os.rename(filename, 'ARCHIVE_'+filename[6:])    # rename the old file to ARCHIVE_$DATE$ on 
                # Store the big tree and queue both locally and on the remote workers:
                cidr_file = 'CIDRS.'+strftime("%Y%m%d%H%M", gmtime())
                queue_file = 'QUEUE.'+strftime("%Y%m%d%H%M", gmtime())
                pickle.dump(tree, open(cidr_file, 'wb'))
                pickle.dump(queue, open(queue_file, 'wb'))
                # put new file on all workers and archive old files with chord
                for i in xrange(self.num_nodes):
                    proxy = xmlrpclib.ServerProxy('http://'+str(self.node_list[i])+':7699')
                    with open(cidr_file, "rb") as handle:
                        proxy.store_file(cidr_file, xmlrpclib.Binary(handle.read()))
                    with open(queue_file, "rb") as handle:
                        proxy.store_file(queue_file, xmlrpclib.Binary(handle.read()))

                    # archive old file on worker after checking with chord
                    proxy.archive_file(filename, 'ARCHIVE_'+filename[6:])

                # store Hash of the new CIDR file and store for future use
                self.CIDRFormerHash = h.hexdigest()

            elif len(oldCidrFilename) > 1:
                self.log('error', 'DistMaster::mergeIndividualPickles('+str(self.iter)+'): More than one CIDRS files found!')
            else:
                self.log('error', 'DistMaster::mergeIndividualPickles('+str(self.iter)+'): No former CIDRS file found. Please verify that this is correct.')
                # Store the big tree and queue both locally and on the remote workers:
                cidr_file = 'CIDRS.'+strftime("%Y%m%d%H%M", gmtime())
                queue_file = 'QUEUE.'+strftime("%Y%m%d%H%M", gmtime())
                pickle.dump(tree, open(cidr_file, 'wb'))
                pickle.dump(queue, open(queue_file, 'wb'))
                # put file on all workers
                for i in xrange(self.num_nodes):
                    proxy = xmlrpclib.ServerProxy('http://'+str(self.node_list[i])+':7699')
                    with open(cidr_file, "rb") as handle:
                        proxy.store_file(cidr_file, xmlrpclib.Binary(handle.read()))
                    with open(queue_file, "rb") as handle:
                        proxy.store_file(queue_file, xmlrpclib.Binary(handle.read()))

                # store Hash of the new CIDR file and store for future use
                self.CIDRFormerHash = h.hexdigest()

        else:
            self.log('notice', 'DistMaster::mergeIndividualPickles('+str(self.iter)+'): Trees are the same.')
            # If the checksums are the same just update the date/time
            oldCidrFilename = glob.glob('CIDR*')
            if len(oldCidrFilename) == 1:
                self.log('notice', 'DistMaster::mergeIndividualPickles('+str(self.iter)+'): Updating the CIDRS file name.')
                filename = oldCidrFilename[0]
                os.rename(filename, filename[:7]+strftime("%Y%m%d%H%M", gmtime()))  # rename the old file with the current date/time
            else:
                self.log('error', 'DistMaster::mergeIndividualPickles('+str(self.iter)+'): No former CIDRS file found. Please verify that this is correct.')
                # Just overwrite
                # Store the big tree and queue both locally and on the remote workers:
                cidr_file = 'CIDRS.'+strftime("%Y%m%d%H%M", gmtime())
                queue_file = 'QUEUE.'+strftime("%Y%m%d%H%M", gmtime())
                pickle.dump(tree, open(cidr_file, 'wb'))
                pickle.dump(queue, open(queue_file, 'wb'))
                # put file on all workers
                for i in xrange(self.num_nodes):
                    proxy = xmlrpclib.ServerProxy('http://'+str(self.node_list[i])+':7699')
                    with open(cidr_file, "rb") as handle:
                        proxy.store_file(cidr_file, xmlrpclib.Binary(handle.read()))
                    with open(queue_file, "rb") as handle:
                        proxy.store_file(queue_file, xmlrpclib.Binary(handle.read()))
        
        self.log('notice', 'DistMaster::mergeIndividualPickles('+str(self.iter)+'): CIDRS.p and QUEUE.p created.')
        self.log('notice', 'DistMaster::mergeIndividualPickles('+str(self.iter)+'): '+str(cidrFilesMerged)+' individual CIDR and '+str(queueFilesMerged)+' Queue files merged.')
    

    # Worker node function
    def WorkerNode(self):
        import select
        print "Worker node at work"
        self.log('notice', 'Worker node at work')

        # open a non-blocking socket and bind to port 6669 and listen
        MasterWorkerSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        MasterWorkerSocket.setblocking(0)
        myAddress = ('%s' % (self.myip), 6669)
        MasterWorkerSocket.bind(myAddress)
        MasterWorkerSocket.listen(1)

        inputs = [ ]
        inputs.append(MasterWorkerSocket)

        outputs = [ ]
        timeout = 300 # 300 sec = 5 min timeout
    
        # Worker Nodes wait for incoming connection from master using
        # select() with timeout. On timeout, return immediately and
        # __main__ will run DistributedCrawler again which will re-run
        # the LeaderElection to start crawling with new Master

        while inputs:
            # wait for incoming connections but with timeout
            read, write, error = select.select(inputs, outputs, inputs, timeout)

            # if timeout, exit
            if not (read or write or error):
                print 'ERROR: WorkerNode('+str(self.myip)+'): timedout waiting for connection from Master'
                self.log('error','WorkerNode('+str(self.myip)+'): timedout waiting for connection from Master')
                # Assume Master node is dead and remove it from node_list
                masternode = '%s' % (self.leader)
                # self.node_list.remove(masternode)
                # print 'WARNING: Worker removing DEAD Master %s from node_list' % (masternode)
                # self.log('warning','Worker removing DEAD Master'+str(masternode)+'from node_list')
                break
            else:
                # else we have a valid connection
                for s in read:
                    if s is MasterWorkerSocket: # new connection
                        # connect with Master
                        MasterWorkerConnection, master = s.accept()
                        print "Worker(%s) received new connection from master(%s)" % (self.myip, master)
                        self.log('notice', 'Worker('+str(self.myip)+') received new connection from master('+str(master)+')')
                        MasterWorkerConnection.setblocking(0)
                        
                        # remove listening socket and add the new connection socket to inputs[]
                        inputs.remove(MasterWorkerSocket)
                        inputs.append(MasterWorkerConnection)
                        # close the listening socket and exit
                        MasterWorkerSocket.shutdown(socket.SHUT_RDWR)
                        MasterWorkerSocket.close()
                    else: # data from existing connection
                        # recv IP range to crawl from Master
                        data = s.recv(1024)
                        if data:
                            print "Worker received data(%s) from master(%s)" % (data, master)
                            self.log('notice', 'Worker received data('+str(data)+') from master('+str(master)+')')

                            dataArray = data.split('|')
                            if len(dataArray)==3:
                                procNum = dataArray[0]
                                firstIP = dataArray[1]
                                lastIP = dataArray[2]
                                self.crawlerResult = ''
                                self.contactCrawlerapp(procNum, firstIP, lastIP)

                                # if crawlerapp crawled successfully, send DONE
                                if self.crawlerResult == 'DONE':
                                    self.contactMaster(procNum, s)

                                # Remove and close Worker-Master connection
                                inputs.remove(s)
                                s.shutdown(socket.SHUT_RDWR)
                                s.close()

                            else:
                                print "ERROR Worker received incorrect data(%s) from master(%s)" % (data, master)
                                self.log('error', 'Worker received incorrect data('+str(data)+') from master('+str(master)+')')

                        else: # Master has disconnected- close socket
                            print "Worker(%s): Master node closed connection. GoodBye!" % (self.myip)
                            self.log('warning','Worker('+str(self.myip)+'): Master node closed connection. GoodBye!')
                            print 'WARNING: Master may be dead- we will detect this in the next crawling run'
                            self.log('warning','WARNING: Master may be dead- we will detect this in the next crawling run')

                            inputs.remove(s)
                            s.shutdown(socket.SHUT_RDWR)
                            s.close()

    # API that contacts Master to send DONE|procNum msg
    def contactMaster(self, procNum, s):
        try:
            msg = 'DONE'+'|'+str(procNum)
            s.sendall(msg)
        except Exception as e:
            print 'ERROR: Worker unable to send DONE to Master'+str(e)
            self.log('error','Worker unable to send DONE to Master'+str(e))
            print 'WARNING: Master may be dead- we will detect this in the next crawling run'
            self.log('warning','WARNING: Master may be dead- we will detect this in the next crawling run')

    # API that contact crawlerapp and returns when crawlerapp finishes
    def contactCrawlerapp(self, procNum, firstIP, lastIP):
        # open a blocking socket and listen for DONE from crawlerapp
        WorkerCrawlerSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # start crawlerapp subprocess
        subprocess.Popen('python crawlerapp.py '+str(procNum)+' '+str(firstIP)+' '+str(lastIP), shell=True)

        # wait for crawlerapp to finish and send DONE
        WorkerCrawlerAddress = ('localhost', 6668)
        WorkerCrawlerSocket.bind(WorkerCrawlerAddress)
        WorkerCrawlerSocket.listen(1)
        while True:
            WorkerCrawlerConn, client_address = WorkerCrawlerSocket.accept()
            try:
                while True:
                    data = WorkerCrawlerConn.recv(1024)
                    if data:
                        # DO SOMETHING WITH THE DATA RECEIVED
                        if data == 'DONE':
                            print 'Crawlerapp crawled successfully'
                            self.log('notice','Crawlerapp crawled successfully')
                            self.crawlerResult = 'DONE'
                            break

                    else:
                        print 'ERROR crawlerapp closed connection'
                        self.log('error','crawlerapp closed connection')
                        self.crawlerResult = 'ERROR'
                        break

            finally:
                # close worker crawler socket
                WorkerCrawlerSocket.close()
                # close Client-Crawler connection
                WorkerCrawlerConn.close()
                break

    # Logging method:
    def log(self, type, data):
        logFile = open('MasterWorkerlogs.log', 'a')
        if type == 'error':
            logFile.write(strftime("%Y-%m-%d %H:%M:%S", gmtime())+" GMT ["+str(os.getpid())+"]<Error>: "+data+"\n")
        elif type == 'notice':
            logFile.write(strftime("%Y-%m-%d %H:%M:%S", gmtime())+" GMT ["+str(os.getpid())+"]<Notice>: "+data+"\n")
        elif type == 'warning':
            logFile.write(strftime("%Y-%m-%d %H:%M:%S", gmtime())+" GMT ["+str(os.getpid())+"]<Warning>: "+data+"\n")
        elif type == 'bench':
            logFile.write(strftime("%Y-%m-%d %H:%M:%S", gmtime())+" GMT ["+str(os.getpid())+"]<Benchmark>: "+data+"\n")
        logFile.close()
    

if __name__ == '__main__':
    # run distributed crawler in an infinite loop
    # WARNING: This will run the crawler infinitely till all python processes
    # on all nodes are killed
    c = DistributedCrawler()
    c.find_nodes()
    c.leader = 0
    curr_iter = c.iter
    c.start_xmlrpc_server() # start XMLRPC server thread
    while True:
        print "Iteration: %d " % (c.iter)

        c.crawler()
        time.sleep(120) # sleep for 120 seconds. Doing this mainly to allow kernel to release the close()d sockets that I need to re-use again

        # After each iteration master will increment iter and only then find active nodes again.
        # If worker nodes detect a failed master, do not call find_nodes() here
        # and workers will elect a new leader from among the remaining nodes
        if curr_iter != c.iter:
            curr_iter = c.iter
            c.find_nodes()
