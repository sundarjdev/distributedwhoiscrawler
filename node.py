from SimpleXMLRPCServer import SimpleXMLRPCServer
from xmlrpclib import Server
from urlparse import urlparse
import threading
import sys


STABILIZE_PERIOD = 1
FIX_FINGER_PERIOD = 1 
NUM_SUCCESSORS = 2
NUM_REPLICATION = 1

SIZE = 5
MAX = 2**SIZE

SimpleXMLRPCServer.allow_reuse_address = 1

def betweenIncludeEnd(ident, begin, end):
    if ident == end :
        return True
    else:
        return between(ident, begin, end);

#was used in previous chord paper    
#def betweenIncludeBegin(ident, begin, end):
#    if ident == begin:
#        return True
#    else:
#        return between(ident, begin, end)    

#returns true if id between begin and end.  It is possible that end is numerically before begin due to the circular nature of problem
def between(ident, begin, end):
    #begin == end means any number, ie is the number of the circle
    if begin == end :
        return True
    else :
        if begin < end:
            if ident < end and ident > begin:
                return True
            else:
                return False
        else:
            offset = MAX - begin
            begin = 0
            end = (end + offset) % MAX
            check = (ident + offset) % MAX
        
            if(check < end and check > begin):
                return True
            else:
                return False

#code based on chord pseudocode in: Chord: A Scalable Peer-to-peer Lookup Service for Internet Applications
#have tried to use method names given in the paper

#general id variables renamed ident because id has specific python meaning
class Node(object):
    def __init__(self, ident, url):
        
        #all ids must fall in range from 0 - MAX - 1
       
        #store my id and my url
        self.ident = ident % MAX
        self.url = url

        self.starts = []
        self.finger = []
        self.keys = []
        self.next = 0;
        
        self.successors = []
        
        self.routing = {}
        self.values = {}
        
        self.run = True
        
        #store my url
        self.routing[ident] = url

    def get_identifier(self) :
        return self.ident

    def set_successor(self, ident, url):
        self.finger[0] = ident
        self.routing[ident] = url
        return 0

    def get_key(self, ident):
        for n in self.keys :
            if n < ident:
                self.keys.remove(n)
                return n 
        return None

    def get_ip_for_key(self, key) :
        if key not in self.keys :
          return None
        else :
          return self.values[key]
    

    #Access function for remote variable lookup
    def get_successor(self):
        return self.finger[0], self.routing[self.finger[0]]
    
    def get_urlForNode(self, ident):
        if ident in self.routing :
            return self.routing[ident]
        else : 
            return 0
    
    def get_successor_list(self):
        return self.successors
    
    def insert_into_successor_list(self, ident, url):
        self.routing[ident] = url
        if ident not in self.successors :
            if len(self.successors) >= NUM_SUCCESSORS :
                self.successors.pop()
            self.successors.append(ident)            
    
    #Access function for remote variable lookup
    def get_predecessor(self):
        if(self.predecessor == None) :
            return self.ident, self.url
        else:
            return self.predecessor, self.routing[self.predecessor]
        
    def get_identification(self):
        return self.ident
        
    def set_predecessor(self, ident, url):
        #print "My predecessor " + str(ident) + " @ " + url
        self.predecessor = ident
        self.routing[ident] = url
        #Need to return something, don't want to deal with Python's Nones
        return 0
    
    def closest_preceding_node(self, ident):
        #print "ClosestPrecedingFinger, ident = " + str(ident)
        for n in range(SIZE-1, -1, -1):
            #print n
            if between(self.finger[n], self.ident, ident) :
                #print "Looking for URL For Node: " + str(self.finger[n])
                #print "closet preceding finger returning " + str(self.finger[n])
                return self.finger[n]
        #print "closet preceding finger returning " + str(self.ident)    
        return self.ident    
    
    def do_add_key(self, ident, ipAddr):
        if ident not in self.keys:
          self.keys.append(ident)
          self.values[ident] = ipAddr

    def add_key(self, ident, ipAddr):
        node, url = self.find_successor(ident)
        
        if node == self.ident :
            s = self
        else :
            s = Server(url)
        
        try:
            s.do_add_key(ident, ipAddr)
        except:
            print "Failed to add key"
            
        numReplication = NUM_REPLICATION;
        
        while numReplication > 0: 
          node, url = s.get_successor()
          if node == self.ident :
            s = self
          else :
            s = Server(url)          
          try:
            s.do_add_key(ident, ipAddr)
          except:
            print "Failed to add replicated key"

          numReplication = numReplication -1           
            
          
    def lookup(self, hashId):
        node, url = self.find_successor(hashId)
  
        print "Node: " + str(node) + " Ident: " + str(hashId)

        if node == self.ident :
            s = self
        else :
            s = Server(url)        
        
        while True:
          try:
              return s.get_ip_for_key(ident) 
          except:
              stabilize()
                 
    
    def leave_chord(self):
        print " "
        print "Leaving chord"
        self.run = False
        
        if self.finger[0] == self.ident :
            return
        else :
            s = Server(self.routing[self.finger[0]])
        
        #hand off my keys
        for n in self.keys :
            s.do_add_key(n, self.values[n])
        
        #notify my successor of new predecessor
        s.set_predecessor(self.predecessor, self.routing[self.predecessor])
        
        #notify my predecessor of possible new successor
        if self.predecessor != self.ident :
            s = Server(self.routing[self.predecessor])
            s.insert_into_successor_list(self.finger[0], self.routing[self.finger[0]])
        
    def find_successor(self, ident):
        #print "FindSuccessor"
        if betweenIncludeEnd(ident, self.ident, self.finger[0]) :
            #return successor
            return self.finger[0], self.routing[self.finger[0]]
        else : 
        
            while True:
            
              nPrime = self.closest_preceding_node(ident)
            
              if nPrime == self.ident:
                  s = self #this should always have been handled above
              else:
                  s = Server(self.routing[nPrime])            
              try:
                  successor, successorUrl = s.find_successor(ident)
                  self.routing[successor] = successorUrl
                  return successor, successorUrl
              except:
                  stabalize()
                        
    def notify(self, ident, url):
        #print "Notify: " + str(self.predecessor) + " " + str(ident) + " " + str(self.ident) 
        if self.predecessor == None :
            self.predecessor = ident
            self.routing[ident] = url
        elif between(ident, self.predecessor, self.ident) :
            self.predecessor = ident
            self.routing[ident] = url

    # Interface to webcrawler.  Tells it if it should keep the archive or not
    def keepData(self, hashId, ipAddr) :
      node = self.find_successor(hashId)[0]
      if node == self.ident :
        self.add_key(hashId, ipAddr)
        return True
      else :
        return False
                                        
                                
    def stabilize(self):
        if not self.run :
            return
               
        if self.finger[0] == self.ident :
            s = self
        else :
            s = Server(self.routing[self.finger[0]])
                
        try:
            x, url = s.get_predecessor()
        except:
            print "*****************CANNOT REACH SUCCESSOR****************"
            while len(self.successors) > 0 :
                nextIdToTry = self.successors[0]
                try:
                    s = Server(self.routing[nextIdToTry])
                    
                    #try an RPC to see if next successor is alive
                    self.finger[0] = s.get_identifier() 
                    threading.Timer(STABILIZE_PERIOD, self.stabilize).start()                    
                    return
                except:
                    self.successors.pop(0)
            
            #this SHOULD never execute.  It would imply that you have 
            #no successors, and can't communicate with yourself.  If
            #that's the case, just shutdown
                                  
            self.run = False
            return
                      
        if(between(x, self.ident, self.finger[0])) :
            self.finger[0] = x
            self.routing[x] = url
                                   
        s.notify(self.ident, self.url)    
        
        successorList = s.get_successor_list()
        if len(successorList) >= NUM_SUCCESSORS :
            successorList.pop()
        if self.finger[0] not in successorList :
            successorList.insert(0, self.finger[0])
        self.successors = successorList
              
        #if(x not in successorListForSuccessor) :
        #    print "Updating successor list"
        #    if len(successorListForSuccessor) >= NUM_SUCCESSORS :
        #        successorListForSuccessor.pop();
        #    successorListForSuccessor.insert(0, x)

        #self.successors = successorListForSuccessor
    
        if self.run == True :
            threading.Timer(STABILIZE_PERIOD, self.stabilize).start()
        
        return 0    
    
    def fix_fingers(self):    
        if not self.run :
            return        
                                   
        i = self.next #from old chord paper where finger fix random random.randint(0, SIZE- 1)
        
        print "Verifying finger " + str(i)
        
        ident, url = self.find_successor(self.starts[i])
        
        self.finger[i] = ident
        self.routing[ident] = url
        
        self.print_node_status()
        
        self.next = self.next + 1;
        if(self.next >= SIZE) : 
            self.next = 0
        
        if self.run == True:
            threading.Timer(FIX_FINGER_PERIOD, self.fix_fingers).start()
        
    #sit here and wait for further instructions/requests
    def main_loop(self):
        hostname = urlparse(self.url).hostname
        port = urlparse(self.url).port
        s = SimpleXMLRPCServer((hostname, port), logRequests=False, allow_none=True)
        s.register_instance(self)
        
        threading.Timer(FIX_FINGER_PERIOD, self.fix_fingers).start()
        threading.Timer(STABILIZE_PERIOD, self.stabilize).start()

        try:
            s.serve_forever()
        except KeyboardInterrupt:
            self.leave_chord()
            self.run = False
            
        return 0
    
    #only for debugging
    def print_node_status(self):
        print "***********************************"
        print "Status for node: " + str(self.ident)
        print "URL: " + self.url
        print "Predecessor: " + str(self.predecessor)
        print "Finger table:"
        for i in range(SIZE):
            print str(self.starts[i]) + " : " + str(self.finger[i])
        print " "
        print "Keys:"
        for i in self.keys:
            print str(i),
        print " "    
        print "Successor List: "
        for p in self.successors:
            print str(p),     
        print " "
        print "***********************************"
        return 0
    
    def setupFingerTable(self):
        counter = 1
        for _i in range(SIZE):  #the _ in front of i notifies the syntax checker that I'm aware that i isn't used
            self.starts.append((self.ident + counter) % MAX)
            self.finger.append(self.ident)
            counter *= 2       
            
    #if this is called with no remote address, then I'm starting a new chord    
    def join(self, remoteAddr=None):
        self.setupFingerTable()
        self.predecessor = None
        
        if remoteAddr == None :
            s = self
        else :
            s = Server(remoteAddr)
        
        successor, url = s.find_successor(self.ident)
        
        self.finger[0] = successor
        self.routing[successor] = url
               
        if remoteAddr != None: 
            s = Server(url)
        
            #get keys
            newKey = s.get_key(self.ident)
            while newKey != None :
                self.keys.append(newKey)
                newKey = s.get_key(self.ident)
                print newKey
                
        
            successorListForSuccessor = s.get_successor_list()
               
            #if(successor not in successorListForSuccessor) :
            #    print "Updating successor list"
            if len(successorListForSuccessor) >= NUM_SUCCESSORS :
                successorListForSuccessor.pop();
                successorListForSuccessor.insert(0, successor)
                                
            self.successors = successorListForSuccessor
        
        if len(self.successors) < NUM_SUCCESSORS :
            self.successors.append(self.ident)

        
        #if remoteAddr == None :
        self.main_loop()
        #else:
        #    self.print_node_status()    
            
                
        return 0
       


 
